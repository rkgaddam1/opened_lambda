'use strict';
var AWS = require('aws-sdk');
AWS.config.update({region: "us-east-2"});
var dynamo =  new AWS.DynamoDB.DocumentClient();

exports.handler = function(event, context, callback) {
    console.log("event: " + JSON.stringify(event.queryParams));
    
    var cb = function(err, data) {
        if(err) {
            context.fail('Failed with response code:401, invalid partner name provided.');
        }
        else {
            console.log("data: " + JSON.stringify(data));
            if(data.Item !== undefined  && data.Item.partnerId !== undefined ) {
                context.done(null, data.Item);
            }
            else {
                context.fail('Failed with response code:401, invalid partner name provided.');
            }
        }
    };
    
    if(!event.queryParams || !event.queryParams.partnerName) {
        context.fail('Failed with response code:401, partner name should be provided.');
    }
    else {
        dynamo.get({TableName:"partner-map", Key:{partnerName:event.queryParams.partnerName.toLowerCase()}}, cb);
    }
}