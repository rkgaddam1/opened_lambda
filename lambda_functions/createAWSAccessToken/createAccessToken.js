'use strict';
var AWS = require('aws-sdk');
AWS.config.update({region: "us-east-2"});
var dynamo = new AWS.DynamoDB.DocumentClient();

exports.handler = function(event, context) {
    console.log("event: " + JSON.stringify(event));
    console.log("generateUUID(): " +generateUUID());

    if(event.learnerId) {
        deleteToken(event, context);
    }
    else {
        context.fail('Failed with response code:401, missing learner id.');
    }
};

var generateUUID = function () {
    var d = new Date().getTime();
    var uuid = 'xxxxxxxx-xxxx-4xxx-yxxx-xxxxxxxxxxxx'.replace(/[xy]/g, function(c) {
        var r = (d + Math.random()*16)%16 | 0;
        d = Math.floor(d/16);
        return (c=='x' ? r : (r&0x3|0x8)).toString(16);
    });
    return uuid;
};

var createAccessToken = function (event, context) {
    var item = {token:  generateUUID(),
                learnerId : event.learnerId,
                headers: {
                    'x-api-key' : event.headers['x-api-key'],
                    'x-access' : event.headers['x-access'] ? event.headers['x-access'] : null
                    }, 
                createDate : Date.now() 
            };

    var cb = function(err, data) {
        if(err) {
            console.log(err);
            context.fail('Failed with response code:500, Unable to save token information');
        } else {
            console.log(item.token);
            context.done(null, {"access_token": item.token,
                                 "expires_in": event.expirationTime });
        }
    };
    var tokenJson = JSON.stringify(item);
    console.log("token: " + JSON.stringify(tokenJson));
    dynamo.put({TableName:"earned-admission-oauth", Item:item}, cb);
}

var deleteToken = function (event, context) {
    var cb = function (err, data) {
        console.log('deleting item in dynamodb');
        if (err) {
            context.faile('Failed with response code:500, error deleting the token');
        } else {
            createAccessToken(event, context);
        }
    };
    dynamo.delete({TableName:"earned-admission-oauth", Key:{learnerId:event.learnerId}}, cb);
};