'use strict';

//require('dotenv').config({ silent: true });
var jwksClient = require('jwks-rsa');
var jwt = require('jsonwebtoken');

function generate_policy(principal_id, effect, resource)
{
    resource = resource.split('/').slice(0,2).join('/')+'/*';
    console.log(resource);
    return {
        principalId: principal_id,
        policyDocument: {
          Version: '2012-10-17',
          Statement: [{
            Action: 'execute-api:Invoke',
            Effect: effect,
            Resource: resource
          }]
        }
    };
}

var getToken = function (headers) {
    var token;

    if (!headers || !headers['Authorization']) {
        throw new Error("Expected 'heades.Authorization' parameter");
    }

    var tokenString = headers['Authorization']; 
    var match = tokenString.match(/^Bearer (.*)$/);
    if (!match || match.length < 2) {
        throw new Error("Invalid Authorization token - '" + tokenString + "' does not match 'Bearer .*'");
    }
    return match[1];
}

var authenticate = function (event, cb) {
    var token = getToken(event.headers);

    var client = jwksClient({
      cache: true,
      rateLimit: true,
      jwksRequestsPerMinute: 10,
      jwksUri: event.stageVariables.JWKS_URI
    });

    var decoded = jwt.decode(token, { complete: true });
    var kid = decoded.header.kid;
    
    client.getSigningKey(kid, function (err, key) {
        if(err) {
            cb(err);
        }
        else {
            var signingKey = key.publicKey || key.rsaPublicKey;
            jwt.verify(token, signingKey, { audience: event.stageVariables.AUDIENCE, issuer: event.stageVariables.TOKEN_ISSUER },
            function (err, decoded) {
                if (err) {
                    cb(err);
                }
                else {
                    cb(null, generate_policy(decoded.sub, 'Allow', event.methodArn));
                }
            });
        }
    });
}

module.exports.handler = function (event, context, callback) {
    if(event.headers.tokentype) {        
        if(event.headers.tokentype === 'AWS') {
            console.log('AWS token');
            if(event.headers.learnerid) {
               context.succeed( generate_policy('user|DUMMY1234567', 'Allow', event.methodArn));
            }
            context.fail("Unauthorized");
        } else if(event.headers.tokentype === 'AUTH0') {
            console.log('AUTH0 token');
            authenticate(event, function (err, data) {
                if (err) {
                    context.fail("Unauthorized");
                }
                context.succeed( data);
            });
        }
        else {
            console.log("Token type "+event.headers.tokentype);
            context.succeed( generate_policy('user|DUMMY1234567', 'Allow', event.methodArn));
        }
    }
    else {
        console.log('No authorization needed');
        context.succeed( generate_policy('user|DUMMY1234567', 'Allow', event.methodArn));
    }
};
