'use strict';
var AWS = require('aws-sdk');
AWS.config.update({region: "us-east-2"});
var dynamo =  new AWS.DynamoDB.DocumentClient();

exports.handler = function(event, context) {
    console.log("validate aws token handler");
    var userAuthToken = getUserAuthToken(event, context);
    if(!event.headers.learnerId) {
        context.fail("Failed with response code:401, You are not provided learner id.");
    }
    var learnerId = event.headers.learnerId;
    
    var cb = function(err, data) {
        if(err) {
            context.fail('Failed with response code:401, Invalid learner id found in the request headers');
        }
        else if(data.Item && data.Item.headers) {
            console.log("data: " + JSON.stringify(data));
            if(data.Item.token !== userAuthToken) {
                context.fail("Failed with response code:401, Invalid Authorization token found in the request headers");
            } else {
                var tokenValid = false;
            
                if (!validateTokenForExpiry(data.Item.createDate, event.expirationTime)) {
                    deleteToken(learnerId, context);
                }  
                else {
                    console.log('token is valid now chekcing the request! ');
                    tokenValid = true;
                    var params  = combineJSON (event.pathParams, event.queryParams);
                    params = combineJSON(params, event.headers);
                    
                    if (validateRequest(data.Item.headers, params) && tokenValid) {
                        context.succeed('Token and request are valid', true);
                    } 
                    else {
                        context.fail("Failed with response code:401, You are not authenticated to make the request");
                    }
                }
            }
        }
        else {
             context.fail("Failed with response code:401, Invalid Authorization token found in the request headers");
        }
    };
    dynamo.get({TableName:"earned-admission-oauth", Key:{learnerId:learnerId}}, cb);
};

function combineJSON(json1, json2) {
    var mergedJson = {};
    var attrname;
    for (attrname in json1) {
        mergedJson[attrname] = json1[attrname]; 
    }
    for (attrname in json2) {
        mergedJson[attrname] = json2[attrname]; 
    }
    return mergedJson;
}

var getUserAuthToken = function(event, context) {
    var headers = event.headers;
    
    if (headers !== undefined && headers.Authorization !== undefined) {
        var userAuthToken =  headers.Authorization;
        userAuthToken = userAuthToken.replace("Bearer ", "");
        console.log("userAuthToken "+userAuthToken);
        return userAuthToken;
    } 
    else {
        context.fail('Failed with response code:401, No Authorization token found in the request headers', null);
    }
};

//token creation header and requesting api header matches or not
var validateRequest = function (headers, options) {
    for (var option in options) {
        var optionValue = options[option]; 
        for (var header in headers) {
            if(headers[header] === optionValue) {
                if((header.toLowerCase() === 'x-access'|| header.toLowerCase() === 'x-api-key' ) && headers[header] === optionValue) {
                    console.log("HEADER is matched");
                    return true;
                }
            }
        }
    }
    console.log("No header matching");
    return false;
};

var validateTokenForExpiry = function(tokenCreateDate, expiryTime) {
    var expiryTimeInMills = expiryTime*1000;
    var tokenExpirationTime = tokenCreateDate + expiryTimeInMills ;
    if (Date.now() <= tokenExpirationTime) {
        return true;
    } 
    return false;
};

var deleteToken = function (learnerId, context) {
    var cb = function (err, data) {
        console.log(err);
        if (err) {
            console.log('error deleting the token', err);
        } else {
            console.log('Token deleted successfully');
        }
        context.fail("Failed with response code:403, token has expired"); 
    };
    console.log("Calling dynamo.deleteItem " + learnerId);
    dynamo.delete({TableName:"earned-admission-oauth", Key:{learnerId:learnerId}}, cb);
};