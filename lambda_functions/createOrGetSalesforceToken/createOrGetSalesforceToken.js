'use strict';
var AWS = require('aws-sdk');
AWS.config.update({region: "us-east-2"});
var https = require('https');
var querystring = require('querystring');
var dynamo =  new AWS.DynamoDB.DocumentClient();

module.exports.handler = function (event, context) {

    var cb = function(err, data) {
        if(err) {
            console.log('Creating a new token in a error condition');
            createAccessToken(event, context);
        }
        else {
            console.log('Record found ' + data.Item);
            if(data.Item && data.Item.application && data.Item.application === 'salesforce') {
                if (!validateTokenForExpiry(data.Item.createDate, event.expirationTime)) {
                    deleteToken(event, context);                    
                }
                else {
                    context.succeed(data.Item.tokenDetails);
                }
            }
            else {
                console.log('Creating a new token since no record found');
                createAccessToken(event, context);
            }
        }
    };
    
    dynamo.get({TableName:"salesforce-openedx-oauth", Key:{application:"salesforce"}}, cb);
    //createAccessToken(event, context);
};

var createTokenInDynamo = function ( tokenDetails, context ) {
    var item = {application:  "salesforce" ,
                tokenDetails: tokenDetails, 
                createDate : Date.now() 
                };

    var cb = function(err, data) {
        console.log('Creating item in dynamodb');
        if(err) {
            context.faile('Failed with response code:500, error creating the SF token.');
        }
        context.succeed(tokenDetails);
    };
    console.log("token: " + JSON.stringify(item));
    dynamo.put({TableName:"salesforce-openedx-oauth", Item:item}, cb);
}

//validate token with expiry time (default 2 hours)
var validateTokenForExpiry = function(tokenCreateDate, expiryTime) {
    var expiryTimeInMills = expiryTime*1000;
    var tokenExpirationTime = tokenCreateDate + expiryTimeInMills ;
    console.log("tokenCreateDatewithExpiation " + tokenExpirationTime);
    console.log("Date.now() " + Date.now());
    if (Date.now() <= tokenExpirationTime) {
        return true;
    } 
    return false;
};

//deleting the expired token and create a new token
var deleteToken = function (event, context) {
    var cb = function (err, data) {
        console.log('deleting item in dynamodb');
        if (err) {
            context.faile('Failed with response code:500, error deleting the SF token.');
        } else {
            createAccessToken(event, context);
        }
    };
    dynamo.delete({TableName:"salesforce-openedx-oauth", Key:{application:"salesforce"}}, cb);
};

//create a new salesforce token and add into dynamodb
var createAccessToken = function (event, context) {
    var postData = querystring.stringify({
        client_id: event.SF_CLIENT_ID,
        client_secret: event.SF_CLIENT_SECRET,
        grant_type : "password",
        username : event.SF_USERNAME,
        password : event.SF_PASSWORD
    });

    var options = {
        host: event.SF_TOKEN_HOST,
        path: '/services/oauth2/token',
        port: event.SF_HOST_PORT,
        method: 'POST',
        headers: {
            'Content-Type':'application/x-www-form-urlencoded',
           'Content-Length': postData.length
       }
    };

    var createAccessTokenCallback = function (res) {
        var result;
        res.on('data', function (data) {
            var tokenDetails = JSON.parse(data);
            if(tokenDetails.access_token) {
                createTokenInDynamo(tokenDetails, context);
            }
            else {
                context.fail("Failed with response code:401, invalid credentials");
            }
        });  
        res.on('error', function (err) {
            context.fail("Failed with response code:401, invalid credentials");
        });
    };

    var req = https.request(options, createAccessTokenCallback);

    // write data to request body
    req.write(postData);
    req.end();
}