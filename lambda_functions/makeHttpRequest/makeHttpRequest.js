'use strict';

var AWS = require('aws-sdk');
var http = require('https');
AWS.config.update({region: "us-east-2"});

var lambda = new AWS.Lambda({
    region: 'us-east-2' //change to your region
});

exports.handler = function(event, context) {
    if(event.headers && event.headers.tokentype && event.headers.tokentype !== 'NONE') {
        console.log('handle authorized request method');
        handleAuthorizedRequests(event, context);
    }
    else {
        console.log('handle no authorization request method');
        //if path contains sign-up or parter details, make direct call
        handleNoAuthorizationRequests(event, context);
    }
};

var handleAuthorizedRequests = function(event, context) {
  if( event.headers.tokentype === 'AWS' ) {
      validateAWSToken(event, context);
  } else if( event.headers.tokentype === 'AUTH0' ) {
      getSalesforceToken(event, context);
  } else {
      context.done('error', 'You are not authenticated to make the request');  
  }
};

var handleNoAuthorizationRequests = function(event, context) {
    console.log(JSON.stringify(event));
  var path = event.resourcePath.toLowerCase();
  var method = event.httpMethod.toLowerCase();
  if(path && method) {
     if(path === '/oea-partner'  && method === 'get') {
         getPartnerInfo(event, context);
     } else if (path === '/oea-learner-application' && method === 'post') {            
         getSalesforceToken(event, context);
     } else {
         context.done('error', 'You are not authenticated to make the request');  
     }
  } else {
      context.done('error', 'You are not authenticated to make the request');  
  }
};

var validateAWSToken = function (event, context) {
  lambda.invoke({
       FunctionName:'validateTokenAndRequest',
       InvocationType: "RequestResponse",
      // LogType: 'Tail',
       Payload: JSON.stringify(event, null, 2)
    }, function(error, data){
         if(error) {
            context.done('error', error); 
            if(data.Paylaod.indexOf('You are not authenticated to make the request') > -1) {
                context.done('error', error); 
            }
         } else {
            console.log('successfully called lambda function '+ JSON.stringify(data));
            if(data.Payload !== undefined && data.Payload.indexOf('Token and request are valid') > -1) {
                getSalesforceToken(event, context);
            } else {
                context.fail(data.Payload);
             }
         }
    });
};

var getPartnerInfo = function (event, context) {
    console.log(JSON.stringify(event));
  lambda.invoke({
       FunctionName:'validateEAPartner',
       InvocationType: "RequestResponse",
      // LogType: 'Tail',
       Payload: JSON.stringify(event, null, 2)
    }, function(error, data){
         if(error) {
            if(data.Paylaod.indexOf('Failed with response code:401') > -1) {
                context.done('error', error); 
            }
         } else {
            console.log('successfully called getPartnerInfo function '+ JSON.stringify(data));
            var jsondata = JSON.parse(data['Payload']);
            console.log(jsondata);
            event.queryParams.partnerId = jsondata.partnerId;
            getSalesforceToken(event, context);
         }
    });
};

var getSalesforceToken = function (event, context) {
  lambda.invoke({
       FunctionName:'createOrGetSalesforceToken',
       InvocationType: "RequestResponse",
      // LogType: 'Tail',
       Payload: JSON.stringify(event, null, 2)
    }, function(error, data){
         if(error) {
            if(data.Paylaod.indexOf('Failed with response code:401') > -1) {
                context.done('error', error); 
            }
         } else {
            console.log('successfully called createOrGetSalesforceToken function '+ JSON.stringify(data));
            var jsondata = JSON.parse(data.Payload);
            console.log(jsondata);
            event.Authorization = jsondata.access_token;
            makeHttpRequest (event, context);
         }
    });
};

var makeHttpRequest = function (event, context) {
    console.log("makeHttpRequest Called ");
    
    if (event.body === undefined) {
       event.body = "";
    }


    var options = {
      host: event.SF_API_HOST,
      path: getResourcePath(event.SF_BASE_PATH+event.resourcePath, event.pathParams, event.queryParams),
      port: event.SF_HOST_PORT,
      method: event.httpMethod,
      body : JSON.stringify(event.body),
      headers: {
        'Authorization':'Bearer ' + event.Authorization,
        'Content-Type':'application/json',
        'Content-Length':Buffer.byteLength(JSON.stringify(event.body))
      }
    };
           
    var callback = function(response) {
       console.log('Status:', response.statusCode);
       console.log('Headers:', JSON.stringify(response.headers));
       var body = '';
       
       response.on('data', function (data) {
          body += data;
       });
      
       response.on('end', function () {
          console.log('Successfully processed HTTP Request');
          console.log('Response body: :', body);
          // If we know it's JSON, parse it
          if (response.statusCode === 204 ) {
                context.succeed(body);
          }
          
          body = JSON.parse(JSON.parse(body));
          
          if (response.statusCode >= 300 ) {
              context.fail("Failed with response code:" + response.statusCode + ", body: " + JSON.stringify(body), null);
          } else {
              var path = event.resourcePath.toLowerCase();
              var method = event.httpMethod.toLowerCase();
              if (path.indexOf('oea-learner-application') !== -1 && method === 'post') {
                  createAWSToken(event, context, body);
              }
              else {
                  context.succeed(body);
              }
          }

       });

       response.on('error', function(exception) {
          console.log('Failed to process HTTP Request');
          console.log('response statusCode', response.statusCode);
       });
    };
    
    console.log(JSON.stringify(options));
    var req = http.request(options, callback);
    
    req.on('error', function(exception) {
       console.log('Failed to process HTTP Request');
       context.fail("Failed with response body: " + exception);
    });
    
    req.write(JSON.stringify(event.body));
    req.end();
};

var createAWSToken = function(event, context, jsonBody) {
    if(jsonBody.id) {
        event.learnerId = jsonBody.id;
        lambda.invoke({
           FunctionName:'createAccessToken',
           InvocationType: "RequestResponse",
          // LogType: 'Tail',
           Payload: JSON.stringify(event, null, 2)
        }, function(error, data){
             if(error) {
                if(data.Paylaod.indexOf('Failed with response code') > -1) {
                    context.done('error', error); 
                }
             } else {
                console.log('successfully called createAWSToken function '+ JSON.stringify(data));
                var jsondata = JSON.parse(data.Payload);
                jsonBody.Authorization = jsondata.access_token;
                context.succeed(jsonBody);
             }
        });
    } else {
       context.succeed(jsonBody);
    }
};

var getResourcePath = function (resourcePath, pathParams, queryParams) {
   var localResourcePath = resourcePath;
   var queryParamStr ='';
   for (var pathParam in pathParams) {
       localResourcePath = localResourcePath.replace ("{"+pathParam+"}", pathParams[pathParam]);
   }
   
   var count = 0;
   
   for(var queryParam in queryParams) {
       var queryParamsStr = '';
       if(queryParams[queryParam] instanceof Object) {
           queryParamsStr = JSON.stringify(queryParams[queryParam]);
       } else {
           queryParamsStr = queryParams[queryParam];
       }

       var queryParamChar = count === 0 ? '?' : '&';
       queryParamStr +=  queryParamChar + queryParam + '=' +  queryParamsStr;
       count ++;
   }

   localResourcePath += queryParamStr;
   console.log("localResourcePath " + localResourcePath);
   return localResourcePath;
};